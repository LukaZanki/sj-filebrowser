$(function(){

    var filemanager = $('.filemanager'),
        breadcrumbs = $('.breadcrumbs'),
        fileList = filemanager.find('.data'),
        currentFilePath = '',
        allarray = [];


    // Start by fetching the file data from scan.php with an AJAX request

    $.get('scan.php', getdata = function(data) {

        var response = [data],
            currentPath = '',
            breadcrumbsUrls = [];

        var folders = [],
            files = [];

        // This event listener monitors changes on the URL. We use it to
        // capture back/forward navigation in the browser.

        $(window).on('hashchange', function(){

            goto(window.location.hash);

            // We are triggering the event. This will execute
            // this function on page load, so that we show the correct folder:

        }).trigger('hashchange');

        // Hiding and showing the search box

        filemanager.find('.search').click(function(){

            var search = $(this);

            search.find('span').hide();
            search.find('input[type=search]').show().focus();

        });


        // Listening for keyboard input on the search field.
        // We are using the "input" event which detects cut and paste
        // in addition to keyboard input.

        filemanager.find('input').on('input', function(e){

            folders = [];
            files = [];

            var value = this.value.trim();

            if(value.length) {

                filemanager.addClass('searching');

                // Update the hash on every key stroke
                window.location.hash = 'search=' + value.trim();

            }

            else {

                filemanager.removeClass('searching');
                window.location.hash = encodeURIComponent(currentPath);

            }

        }).on('keyup', function(e){

            // Clicking 'ESC' button triggers focusout and cancels the search

            var search = $(this);

            if(e.keyCode == 27) {

                    search.trigger('focusout');

            }

        }).focusout(function(e){

            // Cancel the search

            var search = $(this);

            if(!search.val().trim().length) {

                    window.location.hash = encodeURIComponent(currentPath);
                    search.hide();
                    search.parent().find('span').show();

            }

        });

        // Clicking on folders

        fileList.on('click', 'li.folders', function(e){
            e.preventDefault();

            var nextDir = $(this).find('a.folders').attr('href');

            if(filemanager.hasClass('searching')) {

                // Building the breadcrumbs

                breadcrumbsUrls = generateBreadcrumbs(nextDir);

                filemanager.removeClass('searching');
                filemanager.find('input[type=search]').val('').hide();
                filemanager.find('span').show();
            }
            else {
                breadcrumbsUrls.push(nextDir);
            }

            window.location.hash = encodeURIComponent(nextDir);
            currentPath = nextDir;
        });


        // Clicking on breadcrumbs

        breadcrumbs.on('click', 'a', function(e){
            e.preventDefault();

            var index = breadcrumbs.find('a').index($(this)),
                    nextDir = breadcrumbsUrls[index];

            breadcrumbsUrls.length = Number(index);

            window.location.hash = encodeURIComponent(nextDir);

        });


        // Navigates to the given hash (path)

        function goto(hash) {

            hash = decodeURIComponent(hash).slice(1).split('=');

            if (hash.length) {
                var rendered = '';

                // if hash has search in it

                if (hash[0] === 'search') {

                    filemanager.addClass('searching');

                    rendered = searchData(response, hash[1].toLowerCase());

                    if (rendered.length) {
                            currentPath = hash[0];
                            render(rendered);
                    }
                    else {
                            render(rendered);
                    }

                }

                // if hash is some path

                else if (hash[0].trim().length) {

                    rendered = searchByPath(hash[0]);

                    if (rendered.length) {

                            currentPath = hash[0];
                            breadcrumbsUrls = generateBreadcrumbs(hash[0]);
                            render(rendered);

                    }
                    else {
                            currentPath = hash[0];
                            breadcrumbsUrls = generateBreadcrumbs(hash[0]);
                            render(rendered);
                    }

                }

                // if there is no hash

                else {
                    currentPath = data.path;
                    breadcrumbsUrls.push(data.path);
                    render(searchByPath(data.path));
                }
            }
        }

        // Splits a file path and turns it into clickable breadcrumbs

        function generateBreadcrumbs(nextDir){
                var path = nextDir.split('/').slice(0);
                for(var i=1;i<path.length;i++){
                    path[i] = path[i-1]+ '/' +path[i];
                }
                return path;
        }


        // Locates a file by path

        function searchByPath(dir) {
            var path = dir.split('/'),
                demo = response,
                flag = 0;

            for(var i=0;i<path.length;i++){
                for(var j=0;j<demo.length;j++){
                    if(demo[j].name === path[i]){
                        flag = 1;
                        demo = demo[j].items;
                        break;
                    }
                }
            }

            demo = flag ? demo : [];
            return demo;
        }


        // Recursively search through the file tree

        function searchData(data, searchTerms) {

            data.forEach(function(d){
                if(d.type === 'folder') {

                    searchData(d.items,searchTerms);

                    if(d.name.toLowerCase().match(searchTerms)) {
                        folders.push(d);
                    }
                }
                else if(d.type === 'file') {
                    if(d.name.toLowerCase().match(searchTerms)) {
                        files.push(d);
                    }
                }
            });
            return {folders: folders, files: files};
        }


        // Render the HTML for the file manager

        function render(data) {

            var scannedFolders = [],
                scannedFiles = [];

            allarray = [];

            if(Array.isArray(data)) {

                data.forEach(function (d) {

                    if (d.type === 'folder') {
                        scannedFolders.push(d);
                    }
                    else if (d.type === 'file') {
                        scannedFiles.push(d);
                    }

                });

            }
            else if(typeof data === 'object') {

                scannedFolders = data.folders;
                scannedFiles = data.files;

            }


            // Empty the old result and make the new one

            fileList.empty().hide();

            if(!scannedFolders.length && !scannedFiles.length) {
                filemanager.find('.nothingfound').show();
            }
            else {
                filemanager.find('.nothingfound').hide();
            }

            if(scannedFolders.length) {

                scannedFolders.forEach(function(f) {

                    var itemsLength = f.items.length,
                        name = escapeHTML(f.name),
                        icon = '<span class="icon folder"></span>';

                    if(itemsLength) {
                        icon = '<span class="icon folder full"></span>';
                    }

                    if(itemsLength == 1) {
                        itemsLength += ' item';
                    }
                    else if(itemsLength > 1) {
                        itemsLength += ' items';
                    }
                    else {
                        itemsLength = 'Empty';
                    }
                    var folder = $('<li class="folders"><a href="'+ f.path +'" title="'+ f.path +'" class="folders">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');
                    // MENU FOR EACH FILE
                    // Right click
                    // Trigger action when the contexmenu is about to be shown
                    folder.bind("contextmenu", function (event) {

                        // Avoid the real one
                        event.preventDefault();
                        currentFilePath = f.path;
                        // Show contextmenu
                        $(".custom-menu-folder").finish().toggle(100).

                        // In the right position (the mouse)
                        css({
                            top: event.pageY + "px",
                            left: event.pageX + "px"
                        });
                    });

                    folder.appendTo(fileList);
                    allarray.push(folder);
                });

            }

            if(scannedFiles.length) {

                scannedFiles.forEach(function(f) {

                    var fileSize = bytesToSize(f.size),
                        name = escapeHTML(f.name),
                        fileType = name.split('.'),
                        icon = '<span class="icon file"></span>';

                    fileType = fileType[fileType.length-1];

                    icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';

                    var file = $('<li class="files"><a href="'+ f.path+'" title="'+ f.path +'" class="files">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span> <ul class="custom-menu"><li data-action="first">Delete</li><li data-action="second">Rename</li><li data-action="third">Download</li></ul></a></li>'); //<ul class='custom-menu'><li data-action="first">Delete</li><li data-action="second">Rename</li><li data-action="third">Download</li></ul>
                    // MENU FOR EACH FILE
                    // Right click
                    // Trigger action when the contexmenu is about to be shown
                    file.bind("contextmenu", function (event) {

                        // Avoid the real one
                        event.preventDefault();
                        currentFilePath = f.path;
                        // Show contextmenu
                        $(".custom-menu").finish().toggle(100).

                        // In the right position (the mouse)
                        css({
                            top: event.pageY + "px",
                            left: event.pageX + "px"
                        });
                    });
                    file.appendTo(fileList);
                    allarray.push(file);
                });

            }

            // Generate the breadcrumbs

            var url = '';

            if(filemanager.hasClass('searching')){

                url = '<span>Search results: </span>';
                fileList.removeClass('animated');
            }
            else {

                fileList.addClass('animated');

                breadcrumbsUrls.forEach(function (u, i) {

                        var name = u.split('/');

                        if (i !== breadcrumbsUrls.length - 1) {
                                url += '<a href="'+u+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">→</span> ';
                        }
                        else {
                                url += '<span class="folderName">' + name[name.length-1] + '</span>';
                        }

                });

            }

            breadcrumbs.text('').append(url);


            // Show the generated elements

            fileList.animate({'display':'inline-block'});

        }


        // This function escapes special html characters in names

        function escapeHTML(text) {
            return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
        }


        // Convert file sizes from bytes to human readable units

        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes === 0) return '0 Bytes';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }

    });
    // If the document is clicked somewhere
            $(document).bind("mousedown", function (e) {

                // If the clicked element is not the file menu
                if (!$(e.target).parents(".custom-menu").length > 0) {

                    // Hide it
                    $(".custom-menu").hide(100);
                }
                 // If the clicked element is not the folder menu
                if (!$(e.target).parents(".custom-menu-folder").length > 0) {

                    // Hide it
                    $(".custom-menu-folder").hide(100);
                }
            });
            // If the menu element is clicked FOLDER
            $(".custom-menu-folder li").click(function(e){
                // This is the triggered action name
                switch($(this).attr("data-action")) {

                    // A case for each action.
                    case "first":
                        var r = confirm("Are you sure you want to delete this folder?");
                        if (r === true) {
                            $.ajax({
                                type:'POST',
                                url: 'assets/php/managefiles.php',
                                data: {"delete" : currentFilePath},
                                cache: false,
                                async: false,
                                success: function (response) {
                                    alert("Deleted folder successfully. NAME:"+currentFilePath);
                                    $.get('scan.php', getdata);
                                },
                                error: function (response) {
                                    alert("Error deleting folder. NAME:"+currentFilePath);
                                }
                            });
                        }
                        break;
                    case "second":
                        var filename = prompt("Enter new name of this folder");
                        if (!(filename === null)){
                            $.ajax({
                                type:'POST',
                                url: 'assets/php/managefiles.php',
                                data: {"path" : currentFilePath, "rename" : filename},
                                cache: false,
                                async: false,
                                success: function (response) {
                                    alert("Rename successful. NAME:"+currentFilePath);
                                    for (i=0;i<allarray.length;i++){
                                        allarray[i].unbind("contextmenu");
                                    }
                                    fileList.empty().hide();
                                    $.get('scan.php', getdata);
                                },
                                error: function (response) {
                                    alert("Error renaming folder. NAME:"+currentFilePath);
                                }
                            });
                        }
                        break;
                    case "third":
                        var folderName = currentFilePath.split("/").pop().split("/").pop();
                        console.log("folderName: " + folderName);
                        var location = "#files%" + "2F" + folderName;
                        var win = window.open(location, '_blank');
                        if (win) {
                            //Browser has allowed it to be opened
                            win.focus();
                        } else {
                            //Browser has blocked it
                            alert('Please allow popups for this website');
                        }
                        break;
                }

                // Hide it AFTER the action was triggered
                $(".custom-menu-folder").hide(100);
            });

            // If the menu element is clicked FILE
            $(".custom-menu li").click(function(e){
                // This is the triggered action name
                switch($(this).attr("data-action")) {
                    // A case for each action.
                    case "first":
                        var r = confirm("Are you sure you want to delete this file?");
                        if (r === true) {
                            $.ajax({
                                type:'POST',
                                url: 'assets/php/managefiles.php',
                                data: {"delete" : currentFilePath},
                                cache: false,
                                async: false,
                                success: function (response) {
                                    alert("Deleted file successfully. NAME:"+currentFilePath);
                                    $.get('scan.php', getdata);

                                },
                                error: function (response) {
                                    alert("Error deleting file. NAME:"+currentFilePath+response);


                                }
                            });
                        } else {
                            $(".custom-menu").hide(100);
                        }
                        break;
                    case "second":
                        var filename = prompt("Enter new name of this file");
                        if (!(filename === null)){
                            $.ajax({
                                type:'POST',
                                url: 'assets/php/managefiles.php',
                                data: {"path" : currentFilePath, "rename" : filename},
                                success: function (response) {
                                    alert("Rename successful. NAME:"+currentFilePath);
                                    $.get('scan.php', getdata);

                                },
                                error: function (response) {
                                    alert("Error renaming file. NAME:"+currentFilePath+response);

                                }
                            });
                        } else {
                            $(".custom-menu").hide(100);
                        }
                        break;
                    case "third":
                        var base = new String(currentFilePath).substring(currentFilePath.lastIndexOf('/') + 1);
                        if(base.lastIndexOf(".") !== -1)
                            base = base.substring(0, base.lastIndexOf("."));
                        // for non-IE
                        if (!window.ActiveXObject) {

                            var save = document.createElement('a');
                            save.href = currentFilePath;
                            save.target = '_blank';
                            save.download = base || 'unknown';

                            var evt = new MouseEvent('click', {
                                'view': window,
                                'bubbles': true,
                                'cancelable': false
                            });
                            save.dispatchEvent(evt);

                            (window.URL || window.webkitURL).revokeObjectURL(save.href);
                        }

                        // for IE < 11
                        else if ( !! window.ActiveXObject && document.execCommand)     {
                            var _window = window.open(currentFilePath, '_blank');
                            _window.document.close();
                            _window.document.execCommand('SaveAs', true, base || currentFilePath);
                            _window.close();
                        }
                        break;
                    case "fourth":
                        var win = window.open(currentFilePath, '_blank');
                        if (win) {
                            //Browser has allowed it to be opened
                            win.focus();
                        } else {
                            //Browser has blocked it
                            alert('Please allow popups for this website');

                        }
                        break;
                }

                // Hide it AFTER the action was triggered
                $(".custom-menu").hide(100);
            });
});
