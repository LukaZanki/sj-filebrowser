<?php

   if(isset($_POST['delete']))
    {
        $file = $_POST['delete'];
        if (is_dir("../../".$file)){
            rmdir("../../".$file);
        }else {
            $filepath = "../../".$file;
            unlink($filepath);
        }


    }
    if(isset($_POST['create']))
     {
       $target_dir = "../../files/";
       $file = $target_dir . $_POST['dirName'];
         if (file_exists($file)){
           echo '<script language="javascript">';
           echo 'window.alert("Directory ' . $_POST['dirName'] . ' allready exists!");';
           echo 'window.location.href="../../addDirectory.html";';
           echo '</script>';


         } else {

            mkdir($file, 0777, true);
            echo '<script language="javascript">';
            echo 'window.alert("Directory ' . $_POST['dirName'] . ' has been created!");';
            echo 'window.location.href="../../index.html";';
            echo '</script>';
         }


     }
    if(isset($_POST['rename']) && isset($_POST['path']))
    {
        $fileold = $_POST['path'];
        $filenew = $_POST['rename'];
        if (is_dir("../../".$fileold)){
            $extension = pathinfo($fileold, PATHINFO_EXTENSION);
            $filepathold = "../../".$fileold;
            $filepathnew = "../../".str_replace(basename($fileold),"",$fileold).$filenew.$extension;
        }else{
            $extension = pathinfo($fileold, PATHINFO_EXTENSION);
            $filepathold = "../../".$fileold;
            $filepathnew = "../../".str_replace(basename($fileold),"",$fileold).$filenew.".".$extension;
        }
        rename($filepathold,$filepathnew);
    }

    if(isset($_POST["upload"])) {
        $target_dir = "../../files/" . $_POST["directory"] . "/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $getFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if directory exists
        if (!file_exists($target_dir)) {
            if (mkdir($target_dir, 0700)){
                $uploadOk = 1;
            }else {
                echo "Sorry, error uploading file.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size - if larger than 20MB dont upload
        if ($_FILES["fileToUpload"]["size"] > 20000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                echo '<script language="javascript">';
                echo 'window.alert("File uploaded");';
                echo 'window.location.href="../../index.html";';
                echo '</script>';

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
?>
